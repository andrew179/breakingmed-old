$(document).ready(function(){
// Responsive Font-sizes
$('.header-logo .title').fitText(0.8, {minFontSize: '36px', maxFontSize: '67px'});
$('.header-logo .sub-title').fitText(2, {minFontSize: '16px', maxFontSize: '22px'});
$('.quote').fitText(2, {minFontSize: '18px', maxFontSize: '24px'});
$('.agenda-details span').fitText(2, {minFontSize: '15px', maxFontSize: '18px'});
$('.session-info li a').fitText(2.5, {minFontSize: '12px', maxFontSize: '15px'});

// Carousel
$(".owl-carousel").owlCarousel({
	items: 1,
	loop: true,
	dots: true,
	dotsData: true,
    animateOut: 'fadeOut',
    smartSpeed:450
});
});

 